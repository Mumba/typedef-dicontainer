# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.1.1] 15 Jul 2016
- Fix bug in package manifest.

## [0.1.0] 15 Jul 2016
- Initial release.
