# Mumba DiContainer type definition

Type definition for a `DiContainer` interface (compatible with Sandal).

## Installation

```sh
$ npm install --save mumba-typedef-dicontainer
$ typings install -—save dicontainer=npm:mumba-typedef-dicontainer
```

## People

The original author of _Mumba TypeDef DiContainer_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

